class Personne(object):
    info = []

    def __init__(self):
        self._nom = input("Indiquez son nom : ")
        self._age = int(input("Indiquez son âge : "))
        Personne.info.append([self._nom, self._age])

    def _get_nom(self):
        return self._nom

    def _set_nom(self, nom):
        print("La personne s'appel maintenant :", nom)
        self._nom = nom

    nom = property(_get_nom, _set_nom)

    def _get_age(self):
        print("{} âgé de :".format(self._nom))
        return self._age

    def _set_age(self, age):
        print("La personne est maintenant âgé de :", age)
        self._age = age

    age = property(_get_age, _set_age)

    def returnage(self):
        return self._age


class File:
    def __init__(self):
        self._elements = []

    def retirer(self, y):
        if len(self._elements) == 0:
            print("Erreur : la file est vide, on ne peut pas dépiler")
            return None
        else:
            return self._elements.pop(y)

    def ajouter(self, nouvelem):
        self._elements.append(nouvelem)

    def voir(self):
        return self._elements[0]

    def voirtout(self, x):
        return self._elements[x]

    def longueur(self):
        return len(self._elements)

    def trier(self, keyword):
        (self._elements.sort(key=keyword))


# Création des premières personnes
print("\n4 personnes viennent de se rajouter à la file. Veuillez Rentrez leur informations :\n")
p1 = Personne()
p2 = Personne()
p3 = Personne()
p4 = Personne()

#  Rajout des premières personnes
fileDePersonnes = File()
fileDePersonnes.ajouter(p1)
fileDePersonnes.ajouter(p2)
fileDePersonnes.ajouter(p3)
fileDePersonnes.ajouter(p4)

print("\nVoici les personnes présentes dans la file :")
b = 0
for k in range(fileDePersonnes.longueur()):
    print(fileDePersonnes.voirtout(b).age)
    b += 1

print("\nLa première personnes est :", fileDePersonnes.voir().nom)
print("La file à une longeur de :", fileDePersonnes.longueur())

print("\nNous allons maintenant retirer les gens qui ont moins de 18 ans")
c = 0
d = 0
for k in range(fileDePersonnes.longueur()):
    if Personne.info[d][1] < 18:
        fileDePersonnes.retirer(c)
        d += 1
    else:
        c += 1
        d += 1

print("\nLa file de personnes est maintenant :")
b = 0
for k in range(fileDePersonnes.longueur()):
    print(fileDePersonnes.voirtout(b).age)
    b += 1

print("\n4 nouvelles personnes viennet de se rajouter. Veuillez rentrez leur informations :")
p5 = Personne()
p6 = Personne()
p7 = Personne()
p8 = Personne()

fileDePersonnes.ajouter(p5)
fileDePersonnes.ajouter(p6)
fileDePersonnes.ajouter(p7)
fileDePersonnes.ajouter(p8)

print("\nLa file va maintenant être organiser par ordre d'âge croissant")
fileDePersonnes.trier(Personne.returnage)
b = 0
for k in range(fileDePersonnes.longueur()):
    print(fileDePersonnes.voirtout(b).age)
    b += 1

print("\nLa dernière personne va maintenant se retirer")
fileDePersonnes.retirer(-1)
b = 0
for k in range(fileDePersonnes.longueur()):
    print(fileDePersonnes.voirtout(b).age)
    b += 1
